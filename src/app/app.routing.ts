import { Routes } from '@angular/router';

import { LayoutMain,LayoutAppend,LayoutEmpty } from './components/layout/index';

export const ROUTING: Routes = [
  { path: '', component: LayoutMain },
  { path: 'data', component: LayoutMain },
  { path: 'add', component: LayoutAppend },
  { path: 'edit', component: LayoutAppend },
  { path: 'analysis', component: LayoutEmpty },
  { path: 'monitor', component: LayoutEmpty }

]
