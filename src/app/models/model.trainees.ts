export class Trainees {
	id: number;
    name: string;
    grade: string;
	email: string;
	joinedDate: Date=null;
    address: string;
    city:string;
    country:string;
    zip:string;
    subject:string;
}