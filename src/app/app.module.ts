import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import {MatButtonModule, MatCheckboxModule,MatTabsModule} from '@angular/material';
// import {MatTabsModule} from '@angular/material/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';

import { RouterModule, PreloadAllModules } from '@angular/router';
import { ROUTING } from './app.routing';
import { HeaderComponent, FooterComponent } from './components/common/index';
import { LayoutMain,LayoutAppend,LayoutEmpty ,LayoutAction} from './components/layout/index';

@NgModule({
  declarations: [
    AppComponent,
    LayoutMain,
    LayoutAppend,
    LayoutEmpty,
    LayoutAction,
    HeaderComponent,
    FooterComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxPaginationModule,
    MatButtonModule, MatCheckboxModule,MatTabsModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(ROUTING, { useHash: false, preloadingStrategy: PreloadAllModules })
  ],
  entryComponents: [ 
    AppComponent,    
    LayoutMain,
    LayoutAppend,
    LayoutEmpty,
    LayoutAction
  ],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
