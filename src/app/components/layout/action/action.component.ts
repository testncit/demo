import {Component,Output, EventEmitter} from '@angular/core';

 @Component({
     selector:'layout-action',
     templateUrl:'action.component.html'
 })
  
 export class LayoutAction {
    searchedKeyword='';
    @Output() sendKeywordEvent = new EventEmitter<any>();

    checkKey(key){
        console.log("key",this.searchedKeyword);
        this.sendKeywordEvent.emit({
            searchkeyword: this.searchedKeyword
          });
    }
 }