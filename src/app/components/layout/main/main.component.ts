import { Component,Input } from "@angular/core";
import {Trainee} from "../../../models/index";

@Component({
	selector: 'layout-main',
	templateUrl: './main.component.html'
})
 export class LayoutMain {
	searchedKeyword;
	p:number=1;
	trainees = Trainee;
	backUpTrainees = Trainee.map(x => Object.assign({}, x));
	ngOnInit() {
		this.searchedKeyword =+(localStorage.getItem('searchkey'));
		this.filter();
	}
	checkKey(key){
		console.log("key",this.searchedKeyword);
		console.log("backup",this.backUpTrainees);
		this.filter();
		localStorage.setItem('searchkey', this.searchedKeyword);
		
	}
	filter(){
		if(this.searchedKeyword){
			this.trainees = this.backUpTrainees.filter((val) =>
			(val.id)===this.searchedKeyword);
		} else{
			this.trainees=this.backUpTrainees;
		}
	}
 }