import {Component} from '@angular/core';
import { Router } from '@angular/router';
 @Component({
     selector:'demo-header',
     templateUrl:'header.component.html'
 })
  
 export class HeaderComponent {
    constructor(
		private router: Router
	) { }
    changeView(view) {
		this.router.navigate(['/'+view+'']);
	}
 }